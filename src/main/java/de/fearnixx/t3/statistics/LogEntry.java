package de.fearnixx.t3.statistics;

import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;

@Entity(name = "LogEntry")
@Table(name = "EVENT_LOG")
@SuppressWarnings("unused")
public class LogEntry implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ENTRY_ID")
    private long entryID;

    @Column(name = "USER_UID")
    private String userUID;

    @Column(name = "LOG_TIME")
    private Timestamp logTime;

    @Column(name = "EVENT_TYPE")
    private int eventTypeID;

    @Column(name = "RAW_DATA")
    private String rawData;

    @ManyToOne
    @Transient
    private EventType eventType;

    public long getEntryID() {
        return entryID;
    }

    public void setEntryID(long entryID) {
        this.entryID = entryID;
    }

    public String getUserUID() {
        return userUID;
    }

    public void setUserUID(String userUID) {
        this.userUID = userUID;
    }

    public Timestamp getLogTime() {
        return logTime;
    }

    public void setLogTime(Timestamp logTime) {
        this.logTime = logTime;
    }

    public int getEventTypeID() {
        return eventTypeID;
    }

    /*
     * Type-ID is set with the event type
     */
    private void setEventTypeID(int eventTypeID)
    {
        this.eventTypeID = eventTypeID;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public EventType getEventType() {
        return eventType;
    }

    /*
     * Set the event type including the corresponding ID
     */
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
        setEventTypeID(eventType.getTypeId());
    }

    /*
     * Returns a map with the raw data to access values with a key
     */
    public IDataHolder getRawDataMap() {
        HashMap<String,String> dataMap = new HashMap<>();

        for (String node: getRawData().split("\n")) {
            String[] entry = node.split(":");
            dataMap.put(entry[0],entry[1]);
        }

        BasicDataHolder dataHolder = new BasicDataHolder();
        dataMap.forEach(dataHolder::setProperty);

        return dataHolder;
    }
}
