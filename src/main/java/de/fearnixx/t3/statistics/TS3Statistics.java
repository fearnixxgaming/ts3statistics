package de.fearnixx.t3.statistics;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.ITargetClient;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.service.task.ITask;
import de.fearnixx.jeak.service.task.ITaskService;
import de.fearnixx.jeak.teamspeak.EventCaptions;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.t3.statistics.statisticlistener.clientmovement.ClientMovedListener;
import de.mlessmann.confort.LoaderFactory;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.except.LoaderLookupException;
import de.mlessmann.confort.api.except.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

@JeakBotPlugin(id = "statistics")
public class TS3Statistics {

    private static final String DEFAULT_CONFIG_RESSOURCE = "ts3statistics/conf/excludedEvents.json";
    private static final Logger logger = LoggerFactory.getLogger(TS3Statistics.class);

    @Inject
    @PersistenceUnit(name = "statistics")
    private EntityManager entityManager;

    @Inject
    private IEventService eventService;

    @Inject
    private IInjectionService injectionService;

    //Config
    @Inject
    @Config
    private IConfig configRef;
    private IConfigNode config;

    //Events
    private static final List<String> TS3_EVENT_NAMES = new ArrayList<>(); //All event names
    private List<String> excludedEvents = new ArrayList<>(); //Excluded events = no logging
    private Map<String, EventType> eventTypes = new HashMap<>(); //All logged event types

    @Inject
    private IServer server;

    @Inject
    private IDataCache dataCache;

    @Inject
    private ITaskService taskService;

    /**
     * Event-Property for ENTRY_ID of {@link LogEntry}
     */
    public static final String TS3_STATISTICS_LOG_ENTRY_ID = "ts3statistics.log_entry_id";

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {

        if (entityManager == null) {
            logger.warn("Please register persistence unit \"statistics\"!");
            event.cancel();
            return;
        }

        //EventCaptions contains all possible events in String fields.
        //Those events get added to the list of all possible events
        Arrays.stream(EventCaptions.class.getFields()).forEach(
                field -> {
                    if (String.class.isAssignableFrom(field.getType())) {
                        try {
                            TS3_EVENT_NAMES.add((String) field.get(EventCaptions.class));
                        } catch (IllegalAccessException e) {
                            logger.warn("Failed to read field: {}", field.getName(), e);
                        }
                    }
                }
        );

        loadConfig();

        // Load default config.
        if (config.isVirtual()) {
            try {
                final InputStream defaultConfiguration = this.getClass().getClassLoader()
                        .getResourceAsStream(DEFAULT_CONFIG_RESSOURCE);
                if (defaultConfiguration == null) {
                    throw new IOException("Failed to find resource: " + DEFAULT_CONFIG_RESSOURCE);
                }
                URI resourceUri = new URI("classpath:" + DEFAULT_CONFIG_RESSOURCE);

                try (Reader reader = new InputStreamReader(defaultConfiguration)) {
                    configRef.setRoot(LoaderFactory.getLoader("application/json").parse(reader, resourceUri));
                    logger.info("Config loaded from assets");
                }

                saveConfig();
            } catch (ParseException | LoaderLookupException | IOException | URISyntaxException e) {
                logger.error("Failed to load default configuration from resources!", e);
            }
        }


        //The excluded events can be set in a plain list of strings containing the TS3 event
        final IConfigNode excludedEventsNode = config.getNode("excluded_events");
        if (excludedEventsNode.isList()) {
            excludedEventsNode.asList()
                    .stream()
                    .filter(IConfigNode::isPrimitive)
                    .map(IConfigNode::asString)
                    .forEach(excludedEvents::add);
        }

        entityManager.getTransaction().begin();

        //Every possible event name gets queried
        for (String name : TS3_EVENT_NAMES) {
            TypedQuery<EventType> query = entityManager
                    .createQuery("SELECT t FROM EventType t WHERE t.ts3Event = :ts3_name", EventType.class);
            query.setParameter("ts3_name", name);

            List<EventType> types = query.getResultList();
            EventType type = new EventType();

            //If there is no item in the result list of the query the item has to be added to the database
            if (types.isEmpty()) {
                logger.info("Registering '{}' in the database!", name);
                type.setTs3Event(name);
                type.setDescription("default");
                entityManager.persist(type);
                entityManager.flush();
            } else {
                //Since the event types are unique there is only one entry
                type = types.get(0);
            }

            //Only add the event to the list if it is not excluded
            if (!excludedEvents.contains(name)) {
                eventTypes.put(name, type);
            }
        }

        entityManager.getTransaction().commit();

        registerListeners();

        taskService.scheduleTask(
                ITask.builder()
                        .name("notify-longlasting-connections")
                        .interval(1, TimeUnit.HOURS)
                        .runnable(this::notifyLonglastingConnections).build()
        );
    }

    private void notifyLonglastingConnections() {
        final LocalDateTime limit = LocalDateTime.now().minusHours(56);

        dataCache.getClients()
                .stream()
                .filter(client -> client.getLastJoinTime().isBefore(limit))
                .forEach(client ->
                        server.getConnection()
                                .sendRequest(
                                        client.sendMessage("To ensure the validity of your statistics, please reconnect.")
                                )
                );
    }

    private boolean loadConfig() {
        try {
            configRef.load();
        } catch (ParseException | IOException e) {
            logger.error("Failed to load configuration!", e);
            return false;
        }
        config = configRef.getRoot();
        return true;
    }

    private boolean saveConfig() {
        try {
            configRef.save();
        } catch (IOException e) {
            logger.warn("Failed to save configuration!", e);
            return false;
        }
        return true;
    }

    private void registerListeners() {
        eventService.registerListeners(
                injectionService.injectInto(new ClientMovedListener(entityManager, this))
        );
    }

    @Listener(order = Listener.Orders.EARLIER)
    public void onQueryEvent(IQueryEvent.INotification event) {

        //If the config is not set or the incoming event is excluded the event is ignored
        if (config == null) {
            logger.warn("Cannot log events as configuration could not be loaded earlier.");
            return;
        }

        final String eventCaption = event.getCaption();
        if (excludedEvents.contains(eventCaption)) {
            logger.debug("Skipping event: {}", eventCaption);
            return;
        }

        //Get the event type by the event's caption
        EventType type = eventTypes.get(eventCaption);

        //If there is no result the event was not registered
        if (type == null) {
            logger.warn("Event is not registered in the database: {}", eventCaption);
            return;
        }

        //Persist the log entry for this event in the database
        LogEntry logEntry = new LogEntry();

        logEntry.setEventType(type);

        StringBuilder rawData = new StringBuilder();
        event.getValues().forEach(
                (key, value) -> rawData.append(key)
                        .append(":")
                        .append(value)
                        .append("\n"));
        logEntry.setRawData(rawData.toString());

        //If the event is a client-side event the UID of the client gets added to the LogEntry
        if (event instanceof ITargetClient) {
            logEntry.setUserUID(((ITargetClient) event).getTarget().getClientUniqueID());
        }

        synchronized (this) {

            final EntityTransaction transaction = entityManager.getTransaction();

            try {
                transaction.begin();
                entityManager.persist(logEntry);

                //Write event property for other listeners using the LogEntry
                event.setProperty(TS3_STATISTICS_LOG_ENTRY_ID, String.valueOf(logEntry.getEntryID()));

                entityManager.flush();
                transaction.commit();
                logger.debug("Inserted new log entry.");
            } catch (Exception e) {
                logger.warn("Failed to insert log entry. Attempting transaction rollback.", e);
                transaction.rollback();
            }
        }
    }
}
