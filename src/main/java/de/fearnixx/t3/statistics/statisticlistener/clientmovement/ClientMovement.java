package de.fearnixx.t3.statistics.statisticlistener.clientmovement;

import de.fearnixx.t3.statistics.LogEntry;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "ClientMovement")
@Table(name = "CLIENT_MOVEMENT")
@SuppressWarnings("unused")
public class ClientMovement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "CHANNEL_ID")
    private int channelId;

    @Column(name = "REASON")
    private int reason;

    @OneToOne
    @JoinColumn(name = "ENTRY_ID")
    private LogEntry logEntry;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public LogEntry getLogEntry() {
        return logEntry;
    }

    public void setLogEntry(LogEntry logEntry) {
        this.logEntry = logEntry;
    }
}
